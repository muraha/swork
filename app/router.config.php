<?php

// КАКИЕ_ТО ЗНАЧЕНИЯ ПО УМОЛЧАНИЮ (НАПРИМЕР КОНТРОЛЛЕР И МЕТОД ЕСЛИ НЕ УКАЗАН В ОПИСАНИИ УРЛА)
$set['def']['method'] = 'index';
$set['def']['request'] = 'GET';
$set['def']['auth'] = true;
$set['def']['auth_url'] = '/login/';
// ЗАДАЕМ ОБЩИЕ ПРАВИЛА ДЛЯ ПЕРВОЙ И ВТОРОЙ ПЕРЕМЕННОЙ
$set['def']['params'] = array('[0-9]','\w{1,5}');

//  ОПИСАНИЕ ПРАВИЛ ДЛЯ УРЛОВ
$set['urls'][] = array(
    'url' => '',
    'redirect' => '/dashboard/'
);

$set['urls'][] = array(
    'url' => 'login',
    'redirect' => '/dashboard/'
);

$set['urls'][] = array(
    'url' => 'login',
    'auth' => false,
    'contr' => 'Login',
);

$set['urls'][] = array(
    'url' => 'login',
    'request' => 'POST',
    'auth' => false,
    'contr' => 'Login',
    'method' => 'post'
);

$set['urls'][] = array(
    'url' => 'test',
    'auth' => false,
    'contr' => 'test',
    'method' => 'ttt'
);


return $set;
