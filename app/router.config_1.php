<?php

$set = array();

// КАКИЕ_ТО ЗНАЧЕНИЯ ПО УМОЛЧАНИЮ (НАПРИМЕР КОНТРОЛЛЕР И МЕТОД ЕСЛИ НЕ УКАЗАН В ОПИСАНИИ УРЛА)
$set['def'] = array();
$set['def']['method'] = 'index';
$set['def']['request'] = 'GET';
$set['def']['auth'] = true;
$set['def']['auth_url']= '/login';

//  ОПИСАНИЕ ПРАВИЛ ДЛЯ УРЛОВ
$set['urls'] = array();

// Переадрасация со стартовой старницы при наличии авторизации на страницу приложения
$set['urls'][] = array(
    'url' => '',
    'redirect' => '/app_page'
);
// Переадрасация со стартовой старницы при отсутствии авторизации
$set['urls'][] = array(
    'url' => '',
    'auth' => false,
    'redirect' => '/login'
);
/*
$set['urls'][] = array(
    'url' => 'login',
    'redirect' => '/app_page'
);*/

$set['urls'][] = array(
    'url' => 'login',
    'auth' => false,
    'contr' => 'test'
);


$set['urls'][] = array(
    'url' => 'login2',
    'contr' => 'test',
    'method' => 'index2'
);
$set['urls'][] = array(
    'url' => 'test',
    'contr' => 'test',
    'method' => 'ttt'
);

$set['urls'][] = array(
    'url' => 'app_page',
    'contr' => 'app',
);

return $set;
