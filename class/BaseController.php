<?php

class BaseController
{
	// Link to Router
	protected $router;
	
	// Link to Authorization Oblect
	protected $auth;
	
	// Link to temlater
	protected $tpl;
	
	function __construct($router, $auth, $tpl)
	{
		$this->router = $router;
		$this->auth = $auth;
		$this->tpl = $tpl;
	}
	
}

?>
