<?php

class Core {

    private $router;
    private $auth;
    private $tpl;

    function __construct() {
        // ЗАПУСКАЕМ ПРОВЕРКУ АВТОРИЗАУЦИИ ПОЛЬЗОВАТЕЛЯ
        // И ПЕРЕДАЕМ РЕЗУЛЬТАТ В РОУТЕР
        $this->auth = new Authorization();
        $param = array('auth' => $this->auth->is_auth());
        // Иммитация авторизованности
        $param = array('auth' => true);

        $this->router = new Router($param);
        // ПОЛУЧАЕМ ОТРАБОТАННЫЕ ДАННЫЕ ОТ РОУТЕРА
        $router_data = $this->router->get();
        
        // ИММИТУРУЕМ ДАННЫЕ С РОУТЕРА
        //$router_data['contr']='test';
        //$router_data['method']='index';
        // СОЗДАЕМ ОБЪЕКТ ШАБЛОНИЗАТОРА
        $this->tpl = new Templater();

        if ($router_data['contr']) {
            // ПЕРЕДАЕМ ЭТО ВСЕ В КОНСТРУКТОР (НАСЛЕДУЕМЫЙ ОТ БАЗОВОГО)
            $contr = new $router_data['contr']($this->router, $this->auth, $this->tpl);
            // И ВЫЗЫВАЕМ НУЖНЫЙ МЕТОД И ПЕРЕДАЕМ ПАРАМЕТРЫ СПАРСЕНЫЕ ИЗ УРЛА
            $contr->$router_data['method']($router_data['param']);
        } else {
            // ЕСЛИ КОНТРОЛЛЕР НЕ ОПРЕДЕЛЕН
            // НАПРИМЕР ОТДАЕМ СТРАНИЦУ ОШИБКИ
            echo '404';
        }
    }

}
