<?php

class CreateUser
{
	public $tpl;
	function __construct()
	{
		if(isset($_POST['login']) and isset($_POST['pass']) and isset($_POST['vpass']))
		{
			$login=trim(htmlspecialchars($_POST['login']));
			$pass=trim(htmlspecialchars($_POST['pass']));
			$vpass=trim(htmlspecialchars($_POST['vpass']));
			$name=trim(htmlspecialchars($_POST['name']));
			$lastname=trim(htmlspecialchars($_POST['lastname']));
			$email=trim(htmlspecialchars($_POST['email']));
			$pdb=sha1($pass.$login);
			if(!empty($login) and !empty($pass) and $pass===$vpass)
			{
				$sql=new SQL();
				$count=$sql->pdo->prepare("SELECT id FROM user WHERE login=?");
				$count->execute([$login]);
				$num=$count->fetch();
				if(!$num)
				{
					$create=$sql->pdo->prepare("INSERT INTO user (login,pass,name,last_name,email) VALUES (?,?,?,?,?)");
					$create->execute([$login,$pdb,$name,$lastname,$email]);
					$_SESSION['user']=array();
					$_SESSION['user']['id']=$sql->pdo->lastInsertId();
					$_SESSION['user']['login']=$login;
					$_SESSION['user']['name']=$name;
					$_SESSION['user']['lastname']=$lastname;
					$_SESSION['user']['email']=$email;
					header('Location: /user/');
					exit;
				}
			}
		}
		$this->tpl=file_get_contents(DR.'/views/user/create.tpl');
	}
	function html()
	{
		return $this->tpl;
	}
}
?>