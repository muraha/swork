<?php

class Router {

    // Данные результата обработки роутера
    private $data;

    function __construct($param) {
        // ИЗБАЫВЛЯЕМСЯ ОТ ПЕРВОГО СЛЕША
        $uri = substr(URI, 1);

        if ($uri) {
            $uri = explode('/', strtolower(preg_replace('/\/+/', '/', $uri)));
        } else {
            // Стартовая страница
            $uri = array(0 => '');
        }

        // ПОДГРУЖАЕМ ФАЙЛ НАСТРОЙКИ РОУТИНГА
        $set = include DR . '/app/router.config.php';

        // ПРОБЕГАЕМСЯ ПО СПИСКУ УРЛОВ И ПРОВЕРЯЕМ СОВПАДЕНИЕ РАЗДЕЛА
        $urls = $set['urls'];
        foreach ($urls as $key => $val) {
            if ($val['url'] === $uri[0]) {
                if (!array_key_exists('request', $val)) {
                    $val['request'] = $set['def']['request'];
                }
                if ($val['request'] !== $_SERVER["REQUEST_METHOD"]) {
                    continue;
                }
                // Проверяем необходимость авторизации
                if (!array_key_exists('auth', $val)) {
                    $val['auth'] = $set['def']['auth'];
                }

                if ($val['auth']) {
                    //var_dump($param);
                    // Проверяем что пришло с Authorization
                    if ($param['auth']) {
                        if ($val['redirect']) {
                            header("Location: " . $val['redirect']);
                            exit;
                        }
                    } else {
                        $noauth = true;
                        continue;
                        /* if ($set['def']['auth_url']) {
                          header("Location: " . $set['def']['auth_url']);
                          exit;
                          } */
                    }
                } else {
                    if ($param['auth']) {
                        
                    } else {
                        if ($val['redirect']) {
                            header("Location: " . $val['redirect']);
                            exit;
                        }
                        $noauth = false;
                        $this->data = $val;
                        break;
                    }
                }
                $this->data = $val;
                break;
            }
        }
        if ($noauth) {
            if ($set['def']['auth_url']) {
                header("Location: " . $set['def']['auth_url']);
                exit;
            }
        }
        if ($this->data && !array_key_exists('params', $this->data)) {
            $this->data['params'] = $set['def']['params'];
        }
        $this->data['param'] = array();
        $i = 1;
        while (true) {
            if ($uri[$i]) {
                if (!array_key_exists($i - 1, $this->data['params'])) {
                    break;
                }
                echo $this->data['params'][$i - 1];
                if (preg_match('/^' . $this->data['params'][$i - 1] . '$/', $uri[$i])) {
                    $this->data['param'][] = $uri[$i];
                } else {
                    $this->data['param'][] = null;
                }
            } else {
                break;
            }
            $i++;
        }
        if ($this->data && !array_key_exists('method', $this->data)) {
            $this->data['method'] = $set['def']['method'];
        }
    }

    public
            function get() {
        return $this->data;
    }

}

?>
