<?php
class SQL
{
	public $mysqli;
	public $pdo;
	function __construct($ext=false)
	{
		$config=parse_ini_file(DR.'/app/config.ini',true);
		if(!$ext){$ext=$config['default']['extension'];}
		switch($ext)
		{
			case 'pdo': $this->pdo($config['pdo']); break;
			case 'mysqli': $this->mysqli($config['mysqli']); break;
			default : $this->pdo($config['pdo']);
		}
	}
	function mysqli($config)
	{
		$this->mysqli=new mysqli($config['host'],$config['user'],$config['password'],$config['db']);
		if(!$this->mysqli->connect_error)
		{
			return $this->mysqli;
		}
		else
		{
			if(!is_dir(DR."/log/")){mkdir(DR."/log/",true);}
			file_put_contents(DR."/log/error_mysqli",$this->mysqli->connect_errno.": ".$this->mysqli->connect_error."\n",FILE_APPEND | LOCK_EX);
		}
	}
	function pdo($config)
	{
		$dsn=$config['driver'].':host='.$config['host'].';dbname='.$config['db'].';charset='.$config['charset'];
		$this->pdo=new PDO($dsn, $config['user'], $config['password'], $config['options']);
	}
}

?>
