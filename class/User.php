<?php

class User {

    public $tpl;

    function __construct() {
        $this->tpl = str_replace(['{id}', '{login}', '{name}', '{lastname}', '{email}'], $_SESSION['user'], file_get_contents(DR . '/views/user/user.tpl'));
    }

    function html() {
        return $this->tpl;
    }

}
